var config = {
	
  entry: './index.js',

  output: {
	  path: __dirname + '/',
	  filename: 'webpack-bundle.js'
  },

  devServer: {
 	  inline: true,
	  port: 9000
  },
  devtool: "#eval-source-map",
  module: {
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['react']
				}
			},
			{ test: /\.css$/, loader: "style-loader!css-loader" }
		]
  }
}

module.exports = config;