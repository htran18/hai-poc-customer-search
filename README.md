
# hai-poc-customer-search

UI POC, React + Apollo, making graphql request to search customers as user types some characters for customer name in a search box.


Clone me, then

> npm install
> npm start

In the browser, using some kind of extension or plugin, to add MYSAPSSO2 into request header. Its value is from DEV server.

Visit https://localhost:9000

Hopefully, you can see some customer data :)


