import React from "react";
import { render } from "react-dom";

import { ApolloProvider, Query } from "react-apollo";
import gql from "graphql-tag";
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink, concat } from 'apollo-link';
import { setContext } from 'apollo-link-context';

const END_POINT = 'http://coresapdev101.dev.nonprod.atb.ab.com:8002/sap/bc/api/gql/businesspartner?sap-client=510';
const httpLink = new HttpLink({ uri: END_POINT });
const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache()
});

const SearchResults = (criteria) => (
  <Query
    query={gql`
      {
        BPAdvancedSearch(where: {mcName1_CP:"ary*"}, maxHits: 25){
          bpNumber
          firstname
          lastname
        }
      }
    `}
  >
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Got error. Look in debugger.</p>;
      return data.BPAdvancedSearch.map((customer) => (
        <div key={customer.bpNumber}>
          <div>{`${customer.bpNumber} ${customer.firstname} ${customer.lastname}`}</div>
        </div>
      ));
    }}
  </Query>
);

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <ApolloProvider client={client}>
          <div className="App">
            <h2>CRM clients</h2>
            <h4>{END_POINT}</h4>
            <Customers />
          </div>
        </ApolloProvider>
      </div>
    );
  }
}

export default App;