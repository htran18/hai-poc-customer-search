import React from 'react';
import ReactDOM from 'react-dom';
import CustomerSearch from './src/CustomerSearch.js';
import './src/App.css';

ReactDOM.render(
	<div className="App">
	  <h3>POC customer search (graphql query to SAP CRM)</h3>
	  <CustomerSearch />
	</div>,
	document.getElementById('id-app'));
