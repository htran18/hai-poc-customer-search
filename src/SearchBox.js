import React from 'react';
import { AutoComplete } from 'antd';

const Item = (hit) => {
  return (
    <AutoComplete.Option key={hit.id} value={hit.name}>
      <div className="customer">
        <span className="name">{hit.name}</span>
        <span className="dob">{hit.dob}</span>
      </div>
    </AutoComplete.Option>);
}

class SearchBox extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      customers: []
    };
  }

  render() {
    const updateSearchResults = (value) => {
      const hits = [
        { id: "1", name: 'Mickey Van Mouse', dob: '1960-01-31' },
        { id: "2", name: 'Donald de Duck', dob: '1968-03-11' },
        { id: "3", name: 'Walter von Balaya', dob: '1972-11-30' },
      ];
      const customers = hits.map(hit => Item(hit));
      this.setState({ customers });
      
      //const q = "{ users {id name email age} }";
      // const p = 'http://localhost:4000/graphql';
      const q = '{ BPAdvancedSearch(where:{mcName1_CP:"bru*"},maxHits:20) { bpNumber }';
      const p = 'http://coresapdev101.dev.nonprod.atb.ab.com:8002/sap/bc/api/gql/businesspartner?sap-client=510';
      // run the graphpack app for the URL below
      fetch(p, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: JSON.stringify({query: q})
      }).then(res => res.json())
        .then(data => console.log('Results:', data));


    };
    const filterItems = (inputValue, option) => {
      return option.props.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
    };
    return (
      <div>
        <AutoComplete
          style={{ width: "22em" }}
          placeholder="Name, BP#, DOB,..."
          allowClear
          optionLabelProp="value"
          onSearch={updateSearchResults}
          filterOption={filterItems}
        >
          {this.state.customers}
        </AutoComplete>
      </div>
    );
  }
}

export default SearchBox;