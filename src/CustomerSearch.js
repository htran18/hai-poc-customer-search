/* Widget for B&Ag client search: an auto-complete dropdown (from Ant design).
Query hitting CRM data by graphql.
*/

import React from "react";
import { render } from "react-dom";
import { AutoComplete } from 'antd';
import './customer-search.css';

// graphql end point
const endpoint = 'http://coresapdev101.dev.nonprod.atb.ab.com:8002/sap/bc/atb/graphql/businesspartner?sap-client=510';
    
// an item in the dropdown's selections
const Item = (customer) => {
  const id = customer.partner;
  let name;
  if (customer.orgName) {
    name = `${customer.orgName} (${customer.tradename})`;
  } else {
    name = `${customer.personFnam} ${customer.personLnam} (${customer.birthdate})`;
  }
  return (
    <AutoComplete.Option key={id} value={id}>
      <div className="customer" id={id}>
        <span className="name">{name}</span>
        <span className="phone">{customer.telephone}</span>
        <span className="address">{customer.street}</span>
        <span className="id">{id}</span>
      </div>
    </AutoComplete.Option>);
};

class CustomerSearch extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { customers: [], queryTime: 0 };
  }

  search(criteria) {
    this.setState({ customerItems: [] }); // reset search results
    if (!criteria || criteria.length < 3) {
      return;
    }
    const q = `{
      searchClients(SearchCriteria: {clientName: "${criteria.toUpperCase()}"})
      {
        partner
        orgName tradename personFnam personLnam birthdate
        telephone street
      }
    }`;

    const beginQuerying = new Date().getTime();
    fetch(endpoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({query: q})
    }).then(res => res.json()).then(res => {
      const items = res.data.searchClients.map(customer => Item(customer));
      this.setState({
        customerItems: items,
        queryTime: new Date().getTime() - beginQuerying,
      });
    });
  }

  render() {
    return (
      <div>
        <AutoComplete
          style={{ width: "56em" }}
          className="customer-search-autocomplete"
          placeholder="Org/trade name or personal name (min 3 characters)"
          allowClear
          optionLabelProp="id"
          onSearch={(value) => this.search(value)}
        >
          {this.state.customerItems}
        </AutoComplete>
        <div style={{ "margin-top": "10em", "font-size": "200%" }}>
          {`${this.state.queryTime} ms`}
        </div>
      </div>
    );
  }
}

export default CustomerSearch;