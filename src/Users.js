import React from "react";
import { render } from "react-dom";
import ApolloClient from "apollo-boost";
import { ApolloProvider, Query } from "react-apollo";
import gql from "graphql-tag";
import './App.css';

const END_POINT = 'https://eu1.prisma.sh/htran/pql/dev';
const client = new ApolloClient({ uri: END_POINT });

const Users = () => (
  <Query
    query={gql`
      {
        users {
          id
          name
          email
        }
      }
    `}
  >
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Error :(</p>;

      return data.users.map((user) => (
        <div key={user.id}>
          <p>
            {user.id}: {user.name} {user.email}
          </p>
        </div>
      ));
    }}
  </Query>
);




class App extends React.Component {
  render() {
  return (
    <div className="App">
      <ApolloProvider client={client}>
        <div className="App">
          <h2>🎉</h2>
          <Users />
        </div>
      </ApolloProvider>
    </div>
  );
  }
}

export default App;